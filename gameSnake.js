var gameSnake = (function(){
    /**
     @constructor Snake
     @param {array} segments - segments een array met aaneengesloten slangsegmenten
     @description Het laatste element van de segments van de slang wordt de kop van de slang
     */
    function Snake(segments) {
        this.segments = segments;
        this.head = segments[segments.length-1];
        this.colorHead();
    }

    Snake.prototype = {
        /**
         * @method colorHead() -> void
         * @description Kleurt het laatste segment van de slang met kleur HEAD.
         *              Indien het voorlaatste segment kleur HEAD heeft, krijgt deze de kleur SNAKE en krijgt het laatste segment kleur HEAD.
         */
        colorHead : function() {
            var segments = this.segments, head = segments[segments.length-1], beforeHead = segments[segments.length-2];
            head.color = HEAD;
            if (beforeHead.color === HEAD){
                beforeHead.color = SNAKE;
            }
        },
        /**
         @method canMove(direction) -> boolean
         @desc Test of de slang de randen van het canvas dreigt te overschrijden
         @param direction the richting waarin de slang probeert te bewegen
         @return Geeft true als de slang een stap in de aangegeven richting kan bewegen,
         false als geen stap meer kan worden gemaakt in de aangegeven richting
         **/
        canMove : function (direction){
            var canMove = undefined, head = this.head;
            // vertaal direction in juiste dimensies
            // reken een step vooruit in direction
            switch (direction) {
                case LEFT:
                    canMove = head.x >= XMIN+STEP;
                    break;
                case RIGHT:
                    canMove = head.x <= XMAX-STEP;
                    break;
                case UP:
                    canMove = head.y >= YMIN+STEP;
                    break;
                case DOWN:
                    canMove = head.y <= YMAX-STEP;
                    break;
                default:
                    canMove = false;
            }
            // geef resultaat terug
            return canMove;
        },
        /**
         @method doMove(direction) -> void
         @desc verplaatst de slang in de aangegeven richting. Als op de nieuwe positie
         van de kop van de slang zich een voedselelement bevindt, groeit de slang met 1 element.
         @param {string} direction een van de richtingsconstanten LEFT, RIGHT, UP of down
         **/
        doMove : function(direction) {
            // bereken nieuwe positie head
            var xStep = 0,
                yStep = 0;
            // head = this.head, segments = this.segments;

            switch (direction) {
                case LEFT:
                    xStep = -STEP;
                    break;
                case RIGHT:
                    xStep = STEP;
                    break;
                case UP:
                    yStep = -STEP;
                    break;
                case DOWN:
                    yStep = STEP;
                    break;
            }
            var x = this.head.x + xStep;
            var y = this.head.y + yStep;
            var newHead = canvas.createSegment(x, y);
            this.segments.push(newHead);
            // this.head.color = SNAKE;
            this.head = newHead;
            // this.head.color = HEAD;
            this.colorHead();
            if (this.head.collidesWithOneOf(foods)) {
                foods = foods.filter( (food) => !(food.x === newHead.x && food.y === newHead.y) );
            } else {
                this.segments.shift();
            }
        }
    };
    return {
        Snake : Snake
    };
})();